let http = require("http");

const port = 3000;

http.createServer((request,response)=>{

	if (request.url === "/login") {
		response.writeHead(200,{"Content-Type":"text/plain"});
		response.end("Welcome to the login page.");
	}

	else{
		response.end("I'm sorry the page you are looking for connot be found.");
	}

}).listen(port);

console.log("Server is successfully running");